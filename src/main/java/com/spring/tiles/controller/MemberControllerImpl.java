package com.spring.tiles.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.tiles.service.MemberService;
import com.spring.tiles.vo.MemberVO;

// http://localhost:8080/tiles/member/listMembers.do

@Controller("memberController")
public class MemberControllerImpl implements MemberController {
  private static final Logger logger = LoggerFactory.getLogger(MemberControllerImpl.class);
  
  @Autowired
  private MemberService memberService;
  
  private MemberVO memberVO;

  @Override
  @RequestMapping(value = "/member/listMembers.do", method = RequestMethod.GET)
  public ModelAndView listMembers(HttpServletRequest request, HttpServletResponse response) throws Exception {
    String viewName = getViewName(request);
    // String viewName = (String)request.getAttribute("viewName");
    // System.out.println("viewName: " +viewName);
    logger.info("viewName: " + viewName);
    logger.debug("viewName: " + viewName);
    List<MemberVO> membersList = memberService.listMembers();
    ModelAndView mav = new ModelAndView(viewName);
    mav.addObject("membersList", membersList);
    return mav;
  }

  @Override
  @RequestMapping(value = "/member/addMember.do", method = RequestMethod.POST)
  public ModelAndView addMember(@ModelAttribute("member") MemberVO member,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    request.setCharacterEncoding("utf-8");
    memberService.addMember(member);
    ModelAndView mav = new ModelAndView("redirect:/member/listMembers.do");
    return mav;
  }

  @Override
  @RequestMapping(value = "/member/removeMember.do", method = RequestMethod.GET)
  public ModelAndView removeMember(@RequestParam("id") String id,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    request.setCharacterEncoding("utf-8");
    memberService.removeMember(id);
    ModelAndView mav = new ModelAndView("redirect:/member/listMembers.do");
    return mav;
  }

  @RequestMapping(value = { "/member/loginForm.do", "/member/memberForm.do" }, method = RequestMethod.GET)
  // @RequestMapping(value = "/member/*Form.do", method = RequestMethod.GET)
  public ModelAndView form(HttpServletRequest request, HttpServletResponse response) throws Exception {
    String viewName = getViewName(request);
    ModelAndView mav = new ModelAndView();
    mav.setViewName(viewName);
    return mav;
  }

  @Override
  @RequestMapping(value = "/member/login.do", method = RequestMethod.POST)
  public ModelAndView login(@ModelAttribute("member") MemberVO member,
      RedirectAttributes rAttr,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    ModelAndView mav = new ModelAndView();
    memberVO = memberService.login(member);
    if (memberVO != null) {
      HttpSession session = request.getSession();
      session.setAttribute("member", memberVO);
      session.setAttribute("isLogOn", true);
      mav.setViewName("redirect:/member/listMembers.do");
    } else {
      rAttr.addAttribute("result", "loginFailed");
      mav.setViewName("redirect:/member/loginForm.do");
    }
    return mav;
  }

  @Override
  @RequestMapping(value = "/member/logout.do", method = RequestMethod.GET)
  public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
    HttpSession session = request.getSession();
    session.removeAttribute("member");
    session.removeAttribute("isLogOn");
    ModelAndView mav = new ModelAndView();
    mav.setViewName("redirect:/member/listMembers.do");
    return mav;
  }

  @RequestMapping(value = "/member/*Form.do", method = RequestMethod.GET)
  private ModelAndView form(@RequestParam(value = "result", required = false) String result,
      HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    // String viewName = getViewName(request);
    String viewName = (String) request.getAttribute("viewName");
    ModelAndView mav = new ModelAndView();
    mav.addObject("result", result);
    mav.setViewName(viewName);
    return mav;
  }

  private String getViewName(HttpServletRequest request) throws Exception {
    String contextPath = request.getContextPath(); // 아파치 톰캣에 설정되어 있는 서버의 context 경로
    
    // 뷰에서 getRequestURI를 요청하면 뷰 경로가 나온다고 한다.
    // 출처: https://ohjongsung.io/2017/06/06/request-uri-%EC%A0%95%EB%B3%B4-%EC%96%BB%EA%B8%B0
    String uri = (String) request.getAttribute("javax.servlet.include.request_uri");
    
    if (uri == null || uri.trim().equals("")) {
      uri = request.getRequestURI();
    }
    
    int begin = 0;
    if (contextPath != null) {
      begin = contextPath.length();
    }
    
    int end = 0;
    if((end = uri.indexOf(";")) == -1
        && (end = uri.indexOf("?")) == -1
        && (end = uri.lastIndexOf(".")) == -1) {
      end = uri.length();
    }
    
    return uri.substring(begin, end);
  }
}
