package com.spring.tiles.controller.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UploadController {
  private static final String FILE_REPO_PATH = "/upload";

  @RequestMapping(value = "/file/form")
  public String form() {
    System.out.println("asdfasdf");
    return "file/uploadForm";
  }
  
  @RequestMapping(value="/file/upload",method = RequestMethod.POST)
  public ModelAndView upload(MultipartHttpServletRequest multipartRequest,HttpServletResponse response)
      throws Exception {
    multipartRequest.setCharacterEncoding("utf-8");
    Map<String,Object> map = new HashMap<String,Object>();
    Enumeration enu = multipartRequest.getParameterNames(); // File 외의 일반 파라미터의 이름만
    while (enu.hasMoreElements()) {
      String name = (String) enu.nextElement();
      String value = multipartRequest.getParameter(name);
      map.put(name, value);
    }
    
    List<String> fileList = fileProcess(multipartRequest);
    map.put("fileList", fileList);
    ModelAndView mav = new ModelAndView();
    mav.addObject("map", map);
    mav.setViewName("/file/result");
    return mav;
  }

  private List<String> fileProcess(MultipartHttpServletRequest multipartRequest) throws Exception{
    // 웹 Root 밑의 upload 폴더의 절대 경로 가져오기
    String uploadPath = multipartRequest.getSession().getServletContext().getRealPath("/upload");
    System.out.println(uploadPath);
    File uploadDir = new File(uploadPath);
    if(!uploadDir.exists()) {
      uploadDir.mkdirs();
    }
    
    List<String> fileList= new ArrayList<String>();
    Iterator<String> fileNames = multipartRequest.getFileNames();
    while(fileNames.hasNext()){
      String name = fileNames.next();
      MultipartFile mFile = multipartRequest.getFile(name);
      if(mFile.getSize() <= 0){
        continue;
      }
      
      // 파일 이름, 확장자 구하기
      String fileName = mFile.getOriginalFilename();
      String fileExtension = "";
      int dotIdx = -1;
      if((dotIdx = fileName.lastIndexOf('.')) != -1) {
        fileExtension = fileName.substring(dotIdx);
        fileName = fileName.substring(0, dotIdx);
      }
      
      File uploadFile = new File(uploadPath, fileName + fileExtension);
      for(int num = 1; uploadFile.exists(); num++) {
        uploadFile = new File(uploadPath, fileName + "(" + num + ")" + fileExtension);
      }
      
      mFile.transferTo(uploadFile);
      fileList.add(uploadFile.getName());
    }
    return fileList;
  }//end fileProcess()

}
