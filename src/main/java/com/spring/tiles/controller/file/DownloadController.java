package com.spring.tiles.controller.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DownloadController {
  private static final String FILE_REPO_PATH = "/upload";

  @RequestMapping(value = "/file/download")
  public void download(@RequestParam("imageFileName") String imageFileName,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String uploadPath = request.getSession().getServletContext().getRealPath("/upload");
    
    OutputStream out = response.getOutputStream();
    
    File downFile = new File(uploadPath, imageFileName);
    
    response.setHeader("Cache-Control", "no-cache");
    response.addHeader("Content-disposition", "attachment; fileName=" + imageFileName);
    FileInputStream in = new FileInputStream(downFile);
    byte[] buffer = new byte[1024 * 8];
    while (true) {
      int count = in.read(buffer); // 버퍼에 읽어들인 문자개수
      if (count == -1) // 버퍼의 마지막에 도달했는지 체크
        break;
      out.write(buffer, 0, count);
    }
    in.close();
    out.close();
  }
}

