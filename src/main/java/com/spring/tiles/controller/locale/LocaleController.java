package com.spring.tiles.controller.locale;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LocaleController {

  @RequestMapping("/locale/test")
  public String test() {
    System.out.println("localeController");
    return "/locale/test";
  }

  @RequestMapping("/locale/locale")
  public String locale() {
    return "/locale/locale";
  }
}
