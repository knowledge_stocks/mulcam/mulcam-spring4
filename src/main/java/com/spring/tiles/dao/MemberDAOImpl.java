package com.spring.tiles.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.spring.tiles.vo.MemberVO;

@Repository("memberDAO")
public class MemberDAOImpl implements MemberDAO {
  @Autowired
  private SqlSession sqlSession;

  @Override
  public List<MemberVO> selectList() throws DataAccessException {
    List<MemberVO> membersList = null;
    membersList = sqlSession.selectList("mapper.member.selectList");
    return membersList;
  }

  @Override
  public int insert(MemberVO memberVO) throws DataAccessException {
    int result = sqlSession.insert("mapper.member.insert", memberVO);
    return result;
  }

  @Override
  public int delete(String id) throws DataAccessException {
    int result = sqlSession.delete("mapper.member.delete", id);
    return result;
  }

  @Override
  public MemberVO loginById(MemberVO memberVO) throws DataAccessException {
    MemberVO vo = sqlSession.selectOne("mapper.member.loginById", memberVO);
    return vo;
  }
}
