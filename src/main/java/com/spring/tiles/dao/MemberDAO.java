package com.spring.tiles.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.spring.tiles.vo.MemberVO;

public interface MemberDAO {
  public List<MemberVO> selectList() throws DataAccessException;

  public int insert(MemberVO memberVO) throws DataAccessException;

  public int delete(String id) throws DataAccessException;

  public MemberVO loginById(MemberVO memberVO) throws DataAccessException;
}
