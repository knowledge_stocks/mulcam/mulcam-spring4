package com.spring.tiles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.spring.tiles.dao.MemberDAO;
import com.spring.tiles.vo.MemberVO;

@Service("memberService")
@Transactional(propagation = Propagation.REQUIRED)
public class MemberServiceImpl implements MemberService {
  @Autowired
  private MemberDAO memberDAO;

  @Override
  public List<MemberVO> listMembers() throws DataAccessException {
    List<MemberVO> membersList = null;
    membersList = memberDAO.selectList();
    return membersList;
  }

  @Override
  public int addMember(MemberVO member) throws DataAccessException {
    return memberDAO.insert(member);
  }

  @Override
  public int removeMember(String id) throws DataAccessException {
    return memberDAO.delete(id);
  }

  @Override
  public MemberVO login(MemberVO memberVO) throws Exception {
    return memberDAO.loginById(memberVO);
  }
}
